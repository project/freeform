<?php

/**
 * @file
 * Allow users to define their own fields.
 */

/**
 * Implements hook_field_info().
 */
function freeform_field_info() {
  return array(
    'freeform_text' => array(
      'label' => t('Freeform text'),
      'description' => t("A text field where the user defines its label."),
      'settings' => array('allowed_values' => array(), 'allowed_values_function' => ''),
      'default_widget' => 'freeform_text',
      'default_formatter' => 'freeform_text',
    ),
    'freeform_number_units' => array(
      'label' => t('Freeform integer with units'),
      'description' => t("An integer field where the user defines its units."),
      'settings' => array('allowed_values' => array(), 'allowed_values_function' => ''),
      'default_widget' => 'freeform_number_units',
      'default_formatter' => 'freeform_number_units',
    ),
  );
}

/**
 * Implements hook_field_load().
 *
 * Generate the sanitized version of fields so they can be cached.
 */
function freeform_field_load($entity_type, $entities, $field, $instances, $langcode, &$items) {
  if ($field['type'] === 'freeform_text') {
    // @TODO Figure out why the container field is coming in with a settings
    // array that has only 'user_register_form (Boolean) FALSE' in it.
    // Ah actually i guess that makes sense.

    foreach ($instances as $key => $instance) {
      if (!isset($instance['settings']['text_processing'])) {
        $instances[$key]['settings']['text_processing'] = NULL;
      }
    }
    // We can re-use Text module's hook; this works because _text_sanitize()
    // caches per-column.  (Irrelevant text_with_summary is happily ignored.)
    text_field_load($entity_type, $entities, $field, $instances, $langcode, $items);
  }
}

/**
 * Implements hook_field_is_empty().
 */
function freeform_field_is_empty($item, $field) {
  if (!isset($item['value']) || $item['value'] === '') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_widget_info().
 */
function freeform_field_widget_info() {
  return array(
    'freeform_text' => array(
      'label' => t('Text field with label'),
      'field types' => array('freeform_text'),
    ),
    'freeform_number_units' => array(
      'label' => t('Integer with units'),
      'field types' => array('freeform_number_units'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function freeform_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // $element = array();
  switch($instance['widget']['type']) {
    case 'freeform_text':
      $element['label'] = array(
        '#title' => t('Label'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['label']) ? $items[$delta]['label'] : NULL,
        '#size' => 60,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('freeform-label')),
        '#attached' => array('css' => array(drupal_get_path('module', 'freeform') . '/freeform_admin.css')),
      );
      $element['value'] = array(
        '#title' => t('Text'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
        '#size' => 32,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('freeform-value')),
      );
      break;

    case 'freeform_number_units':
      $element['value'] = array(
        '#title' => t('Number'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
        '#size' => 16,
        '#maxlength' => 20,
        '#attributes' => array('class' => array('freeform-value')),
      );
      $element['units'] = array(
        '#title' => t('Units'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]['units']) ? $items[$delta]['units'] : NULL,
        '#size' => 32,
        '#maxlength' => 255,
        '#attributes' => array('class' => array('freeform-units')),
        '#attached' => array('css' => array(drupal_get_path('module', 'freeform') . '/freeform_admin.css')),
      );
      // Add prefix and suffix.
      if (!empty($instance['settings']['prefix'])) {
        $prefixes = explode('|', $instance['settings']['prefix']);
        $element['#field_prefix'] = field_filter_xss(array_pop($prefixes));
      }
      if (!empty($instance['settings']['suffix'])) {
        $suffixes = explode('|', $instance['settings']['suffix']);
        $element['#field_suffix'] = field_filter_xss(array_pop($suffixes));
      }
      // Validate that we are getting a number and prepare it for the database.
      $element['#element_validate'][] = 'freeform_number_units_widget_validate';
      break;
  }

  return $element;
}

/**
 * FAPI validation of an individual number element.
 *
 * We do not use hook_field_validate() because we clean and set the value.
 */
function freeform_number_units_widget_validate($element, &$form_state) {
  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);

  $value = $element['value']['#value'];
  
  // Interim fake-out until we either add this option or stop doing decimals.
  if (!isset($field['settings']['decimal_separator'])) {
    $field['settings']['decimal_separator'] = '.';
  }

  if (!empty($value)) {
    // Strip invalid characters.
    $regexp = '@[^-0-9\\' . $field['settings']['decimal_separator'] . ']@';
    $value = preg_replace($regexp, '', $value);
    // There needs to be something left after the invalid values are stripped.
    if ($value === '') {
      $message = t('Only numbers and the decimal separator (@separator) allowed in %field.', array('%field' => $instance['label'], '@separator' => $field['settings']['decimal_separator']));
      form_error($element, $message);
    }
    else {
      // Verify that only one decimal separator exists in the field.
      if (substr_count($value, $field['settings']['decimal_separator']) > 1) {
        $message = t('%field: There should only be one decimal separator (@separator).',
          array(
            '%field' => t($instance['label']),
            '@separator' => $field['settings']['decimal_separator'],
          )
        );
        form_error($element, $message);
      }
      else {
        // Substitute the decimal separator; things should be fine.
        $value = strtr($value, $field['settings']['decimal_separator'], '.');
      }
    }
    form_set_value($element['value'], $value, $form_state);
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function freeform_field_formatter_info() {
  return array(
    'freeform_text_plain' => array(
      'label' => t('Plain text'),
      'field types' => array('freeform_text', 'freeform_text_long'),
    ),
    'freeform_text_trimmed' => array(
      'label' => t('Trimmed'),
      'field types' => array('freeform_text', 'freeform_text_long'),
      'settings' => array('trim_length' => 600),
    ),
    'freeform_number_units' => array(
      'label' => t('Integer with units'),
      'field types' => array('freeform_number_units'),
      'settings' => array(
        'thousand_separator' => ',',
        'decimal_separator' => '.',
        'scale' => 0,
        'prefix_suffix' => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function freeform_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  if ($field['type'] === 'freeform_number_units') {
    // We can re-use the formatter settings form number module.
    return number_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  }
  // Else, it is one of our text fields.
  // We can re-use the trim settings form from Text module.
  return text_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function freeform_field_formatter_settings_summary($field, $instance, $view_mode) {
  if ($field['type'] === 'freeform_number_units') {
    // We can re-use the formatter settings form number module.
    return number_field_formatter_settings_summary($field, $instance, $view_mode);
  }
  // Else, it is one of our text fields.
  // We can re-use the trim settings summary from Text module.
  return text_field_formatter_settings_summary($field, $instance, $view_mode);
}

/**
 * Implements hook_field_formatter_view().
 */
function freeform_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $add_label = FALSE;

  switch ($display['type']) {
    case 'freeform_text_default':
    case 'freeform_text_trimmed':
      foreach ($items as $delta => $item) {
        if (!isset($instance['settings']['text_processing'])) {
          $instance['settings']['text_processing'] = NULL;
        }
        $output = _text_sanitize($instance, $langcode, $item, 'value');
        if ($display['type'] == 'freeform_text_trimmed') {
          $output = text_summary($output, $instance['settings']['text_processing'] ? $item['format'] : NULL, $display['settings']['trim_length']);
        }
        $element[$delta] = array('#markup' => $output);
      }
      $add_label = TRUE;
      break;

    case 'freeform_text_plain':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => strip_tags($item['value']));
      }
      $add_label = TRUE;
      break;

    case 'freeform_number_units':
      $settings = $display['settings'];
      foreach ($items as $delta => $item) {
        $output = number_format($item['value'], $settings['scale'], $settings['decimal_separator'], $settings['thousand_separator']);
        $units = check_plain($item['units']);
        $output .= ' ' . $units;
        if ($settings['prefix_suffix']) {
          $prefixes = isset($instance['settings']['prefix']) ? array_map('field_filter_xss', explode('|', $instance['settings']['prefix'])) : array('');
          $suffixes = isset($instance['settings']['suffix']) ? array_map('field_filter_xss', explode('|', $instance['settings']['suffix'])) : array('');
          $prefix = (count($prefixes) > 1) ? format_plural($item['value'], $prefixes[0], $prefixes[1]) : $prefixes[0];
          $suffix = (count($suffixes) > 1) ? format_plural($item['value'], $suffixes[0], $suffixes[1]) : $suffixes[0];
          $output = $prefix . $output . $suffix;
        }
        $element[$delta] = array('#markup' => $output);
      }
      $renderable = $element;
      break;

  }

  if ($add_label) {
    foreach ($items as $delta => $item) {
      // @TODO get $display values from settings, not inherit from parent field.
      $instance['settings']['text_processing'] = NULL;
      $label = _text_sanitize($instance, $langcode, $item, 'label');
      $machine_label = preg_replace('/[^a-z0-9_]+/', '_', strtolower(strip_tags($label)));
      $info = array(
        '#theme' => 'field',
        '#weight' => $display['weight'],
        '#title' => $label,
        '#access' => TRUE,
        '#label_display' => $display['label'],
        '#view_mode' => 'full', // @TODO see if we can get $view_mode,
        '#language' => $langcode,
        '#field_name' => $field['field_name'] . '__' . $machine_label,
        '#field_type' => 'freeform__' . $field['type'],
        '#field_translatable' => $field['translatable'],
        '#entity_type' => $entity_type,
        '#bundle' => $instance['bundle'],
        '#object' => $entity,
        '#items' => array(0 => $element[$delta]),
        'items' => array(0 => $element[$delta]),
     //   '#items' => array('delta' => array('#markup' => 'string')),
      //  '#formatter' => $display['type']
     //   'items' => array($element[$delta]),
     //   '#value' => 'hi mom',
      );
      $info = array('#markup' => drupal_render($info)); // $label . $element[$delta]['#markup']);
  // print_r($info);

  /*
      $element[$delta] = array_merge($info, $element[$delta]);
      $renderable[$delta] = $info;
      $renderable['items'] = $info;
  */
      // NONE OF THE ABOVE FANCINESS IS QUITE WORKING (children items get lost).
      // So we will just mock it up quick-and-ugly for now.  @TODO fix!
      // Machine label will have underscores which is legal but not Drupalesque.
      $renderable[] = array('#markup' => '<div class="field field-name-' . $field['field_name'] . 'field-freeform-' . $machine_label . ' field-type-link-field field-label-inline clearfix"><div class="field-label">' . $label . ':&nbsp;</div><div class="field-items"><div class="field-item even">' . $element[$delta]['#markup'] . '</div></div></div>');
    }
  }

  return $renderable; // $element;
}
